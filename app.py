from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/")
def index():
    fahrenheit = request.args.get("fahrenheit", "")
    if fahrenheit:
        celsius = celsius_from(fahrenheit)
    else:
        celsius = ""

    return (
        	"""<h2> Converta Fahrenheit para Celsius! </h2>"""
		"""<br>"""
		"""<form action="" method="get">
                <input type="text" name="fahrenheit">
                <input type="submit" value="Convert">
            </form>"""
        + "Celsius: "
        + '<a id="celsius">' +celsius+ '</a>'

    )
 
@app.route("/<int:fahrenheit>")
def celsius_from(fahrenheit):
    """Convert Fahrenheit to Celsius degrees."""
    celsius = (float(fahrenheit) - 32 )* 5/9
    celsius = round(celsius, 2)
    return str(celsius)
    
@app.route("/<string:script>")
def run(script):
    script=request.args.get("script", "")
    return (
	"""<h2> Run! 🕸 </h2>"""
	"""<form action="" method="get">
                <input type="text" name="script">
                <input type="submit" value="Run">
            </form>"""
    + '<a id="script">' + script + '</a>'
) 

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
     #app.run(host="0.0.0.0", port=8000, debug=False)